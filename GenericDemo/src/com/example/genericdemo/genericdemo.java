package com.example.genericdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.genericdemo.gdpreferences.Preferences;

public class genericdemo extends Activity {
    private TextView label1;
    private Button button1;
    private CheckBox checkbox1;
    private int b1_cnt=0;
    private int INPUT_REQUEST =7;
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        label1 = (TextView) findViewById(R.id.TextView01);
        button1 = (Button) findViewById(R.id.Button01);
        checkbox1 = (CheckBox) findViewById(R.id.CheckBox01);
    }
    
    public void onButton1Click(View view){
    	switch(view.getId()){
    	case R.id.Button01:
    		label1.setText(String.valueOf( b1_cnt));
    		b1_cnt++;
    		if(button1.getText()==this.getString(R.string.app_playing)){
    			button1.setText(this.getString(R.string.app_paused));
        		
    		}else{
    			button1.setText(this.getString(R.string.app_playing));
    		}   		
    		break;   	
    	}    	
    }
    public void onPrefButtonClick(View view){
    	switch (view.getId()){
    	case R.id.prefButton:
    		label1.setText("pref");
    		startActivityForResult(new Intent(this, Preferences.class), INPUT_REQUEST );
    	}
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
    	super.onActivityResult(requestCode, resultCode, data); 
    	String input = data.getStringExtra("input");
    	label1.setText("Selamlar "+input);
	}
    public void onCeckBox1Click(View view){
    	switch(view.getId()){
    	case R.id.CheckBox01:
    		if (checkbox1.isChecked()){
    			label1.setText("checked");
    		}else{
    			label1.setText("unchecked");
    		}    		
    		break;
    	}
    }
    
}

