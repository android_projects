package com.example.genericdemo.gdpreferences;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.genericdemo.R;

public class Preferences extends Activity{
	private TextView label1;
	private EditText edittext1;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preferences);
        label1 = (TextView) findViewById(R.id.pTextView01);
        edittext1 =(EditText) findViewById(R.id.pEditText01);
	}
	public void onPOkButtonClick(View view){
		switch(view.getId()){
		case (R.id.pOkButton):
			Intent data= new Intent();
			String s=edittext1.getText().toString();
			data.putExtra("input",s);
			label1.setText(s);	
			setResult(Activity.RESULT_OK,data);
			finish();
			
		break;
		}
	}

}
