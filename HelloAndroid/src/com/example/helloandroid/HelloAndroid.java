package com.example.helloandroid;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.view.View;


public class HelloAndroid extends Activity {
	private TextView label;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        label = (TextView) findViewById(R.id.TextView01);
        
    }
    
    public void onButton1Click(View view) {
		switch (view.getId()) {
		case R.id.Button01:
			label.setText("selam");
			break;
		}
    }
    
}